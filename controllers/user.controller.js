/* 
Imports
*/
const Models = require('../models/index');
//

/*  
Methods
*/
    const readAll = () => {
        return new Promise( (resolve, reject) => {
            Models.user.find()
            .exec( (err, data) => {
                if( err ){ return reject(err) }
                else{ return resolve(data) }
            })
        })
    }

    const readOne = id => {
        return new Promise( (resolve, reject) => {
            Models.user.findById( id )
            .exec( (err, data) => {
                if( err ){ return reject(err) }
                else{ return resolve(data) }
            })
        })
    }

    const deleteOne = req => {
        return new Promise((resolve, reject) => {

            Models.user.findById(req.body.id)
            .then(user => {
                // Delete object
                Models.user.findByIdAndDelete(req.body.id, (err, deleted) => {
                    if (err) { return reject(err) }
                    else { return resolve(deleted) };
                })

            })
            .catch(err => reject(err));
        });
    }

    const updateOne = req => {
        return new Promise( (resolve, reject) => {
            // Update object
            Models.user.findById( req.params.id )
            .then( user => {
                user.givenName = req.body.givenName;
                user.familyName = req.body.familyName;
                user.image = req.body.image;
                user.dateModified = new Date();

                user.save()
                .then(updatedUser => resolve(updatedUser))
                .catch(updateError => reject(updateError))
            })
            .catch( err => reject(err) )
        })
    }

/* 
Export controller methods
*/
    module.exports = {
        readAll,
        readOne,
        updateOne,
        deleteOne
    }
//
