/* 
Imports
*/
const { session } = require('passport');
const Models = require('../models/index');
//

/*  
CRUD methods
*/
const createOne = req => {
    return new Promise((resolve, reject) => {
        Models.comment.create(req.body)
            .then(data => { resolve(data) })
            .catch(err => reject(err, {err: err}) )
    })
}

const readAll = () => {
    return new Promise((resolve, reject) => {
        Models.comment.find()
            .populate('author', ['-password'])
            .exec((err, data) => {
                if (err) { return reject(err) }
                else { return resolve(data) }
            })
    })
}

const readOne = id => {
    return new Promise((resolve, reject) => {
        Models.comment.findById(id)
            .populate('author', ['-password'])
            .exec((err, data) => {
                if (err) { return reject(err) }
                else { return resolve(data) }
            })
    })
}

const readAllFromMovie = MovieId => {
    return new Promise((resolve, reject) => {
        Models.comment.find({ movie: movieId })
            .populate('comment', ['content', 'title', 'author'])
            .populate('author', ['-password'])
            .exec((err, data) => {
                if (err) { return reject(err) }
                else { return resolve(data) }
            })
    })
}

// const deleteOne = req => {
//     return new Promise((resolve, reject) => {

//         Models.comment.findById(req.body.id)
//             .then(comment => {

//                 // check comment's author
//                 if (String(comment.author) !== String(req.user._id)) {
//                     reject('User not authorized')
//                 }

//                 // Delete object
//                 Models.comment.findByIdAndDelete(req.body.id, (err, deleted) => {
//                     if (err) { return reject(err) }
//                     else { return resolve(deleted) };
//                 })

//             })
//             .catch(err => reject(err));
//     });
// }
//

module.exports = {
    readAll,
    readOne,
    readAllFromMovie,
    createOne,
    // updateOne,
    // deleteOne
}
//
