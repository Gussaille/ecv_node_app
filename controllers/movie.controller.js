/* 
Imports
*/
const { session } = require('passport');
const Models = require('../models/index');
//

const readAll = () => {
    return new Promise((resolve, reject) => {
        
        Models.movie.find()
        .populate('comments')
        .exec((err, data) => {
            if (err) { return reject(err) }
            else { return resolve(data) }
        })
    })
}

const readOne = (id) => {
    return new Promise((resolve, reject) => {
        Models.movie.findById(id)
        .populate('comments')
        .exec((err, data) => {
            if (err) { return reject(err) }
            else { return resolve(data) }
        })
    })
}

module.exports = {
    readAll,
    readOne
}
//
