const Controllers = {
    auth: require('./auth.controller'),
    user: require('./user.controller'),
    comment: require('./comment.controller'),
    movie: require('./movie.controller'),
}

module.exports = Controllers;
