const Models = require('../models/index');
const bcrypt = require('bcryptjs');
const session = require('express-session');


const register = req => {
    return new Promise( async (resolve, reject) => {
        req.body.password = await bcrypt.hash( req.body.password, 10 );

        // Register new user
        Models.user.create( req.body )
        .then( data => resolve(data) )
        .catch( err => reject(err) );
    })
}

const login = (req, res) => {
    return new Promise( (resolve, reject) => {
        Models.user.findOne( { email: req.body.email }, (err, data) => {
            // Checking if email is already registered
            if( err || data === null ){ 
                return reject("Email invalide");
            } 
            else{
                // Password validation
                const validatedPassword = bcrypt.compareSync( req.body.password, data.password );
                if( !validatedPassword ){ return reject('Mot de passe invalide') }
                else{
                    // Generating user JsonWebToken
                    const userJwt = data.generateJwt(data);
                    
                    // Set cookie response
                    res.cookie( process.env.COOKIE_NAME, userJwt, { maxAge: 700000, httpOnly: true } )

                    // Return User data
                    return resolve(data)
                };
            }
        })
    })
}
const logout = (req, res) => {
    return new Promise( (resolve, reject) => {
        return resolve(true)
    })
}


module.exports = {
    register,
    login,
    logout,
}
