/* 
Imports
*/
    // NPM modules
    require('dotenv').config(); 
    const express = require('express');
    const bodyParser = require('body-parser');
    const cookieParser = require('cookie-parser');
    const passport = require('passport');
    const path = require('path');
    const cors = require('cors');
    const session = require('express-session');

    // Services
    const MONGOclass = require('./services/mongo.class');
//

/*
    Server class
*/
class ServerClass{
    constructor(){
        this.server = express();
        this.port = process.env.PORT;
        this.MongoDB = new MONGOclass;
    }

    init(){
        this.server.use(cors({
            origin: [
              'http://localhost:8080'
            ],
            credentials: true,
            exposedHeaders: ['set-cookie']
        }));

        // Set server view engine
        this.server.set( 'view engine', 'ejs' );

        // Static path configuration
        this.server.set( 'views', __dirname + '/www' );
        this.server.use( express.static(path.join(__dirname, 'www')) );

        // Body-parser
        this.server.use(bodyParser.json({limit: '10mb'}));
        this.server.use(bodyParser.urlencoded({ extended: true }));

        // CookieParser for serverside cookies
        this.server.use(cookieParser(process.env.COOKIE_SECRET));

        // Start Server config
        this.config();
    }

    config(){
        // Set Authentication
        const { setAuthentication } = require('./services/auth.service');
        setAuthentication(passport);

        // Set AUTH router
        const AuthRouterClass = require('./routers/auth.router');
        const authRouter = new AuthRouterClass( { passport } );
        this.server.use('/auth', authRouter.init());

        // Set API router
        const ApiRouterClass = require('./routers/api.router');
        const apiRouter = new ApiRouterClass( { passport } );
        this.server.use('/api', apiRouter.init());

        // Set BACKEND Router
        const BackendRouterClass = require('./routers/backend.router')
        const backendRouter = new BackendRouterClass( { passport } );
        this.server.use('/', backendRouter.init());

        this.launch();
    }

    launch(){
        //Mongo Connection
        this.MongoDB.connectDb()
        .then( db => {
            this.server.listen(this.port, () => {
                // console.log({
                //     node: `http://localhost:${this.port}`,
                //     mongo: db.url,
                // });
            });
        })
        .catch( dbErr => console.log('MongoDB Error', dbErr));
    }
}

const NODE_APP = new ServerClass();
NODE_APP.init();
