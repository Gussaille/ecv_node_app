const Mandatory = {
    register: [ 'givenName', 'familyName', 'password', 'email' ],
    login: [ 'password', 'email' ],
    comment: [ 'movieId', 'title', 'content' ],
    user: [ 'givenName', 'familyName', 'image'],
} 

module.exports = Mandatory;