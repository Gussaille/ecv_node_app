const JwtStrategy = require('passport-jwt').Strategy;
const UserModel = require('../models/user.model');


// Extract token from cookie
const cookieExtractor = (req, res) => {
    let token = null;
    if( req && req.cookies){ token = req.cookies[process.env.COOKIE_NAME] }
    return token;
}

// JsonWebToken authentication
const authJwt = passport => {
    // JsonWebToken passport options 
    const opts = {
        jwtFromRequest: cookieExtractor, 
        secretOrKey: process.env.JWT_SECRET
    }

    // JsonWebToken strategy
    passport.use(new JwtStrategy(opts, (jwtPayload, done) => {
        UserModel.findOne({ _id: jwtPayload._id }, (err, user) => {
            if (err) { return done(err, false)}
            if (user) { return done(null, user) }
            else { return done(null, false) }
        });
    })); 
}

module.exports = {
    setAuthentication: (passport) => {
        authJwt(passport)
    }
}
