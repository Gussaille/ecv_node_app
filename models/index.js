const Models = {
    user: require('./user.model'),
    comment: require('./comment.model'),
    movie: require('./movie.model')
} 

module.exports = Models;