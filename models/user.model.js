const mongoose = require('mongoose');
const { Schema } = mongoose;
const jwt = require('jsonwebtoken');

const MySchema = new Schema({
    '@context': { type: String, default: 'http://schema.org' },
    '@type': { type: String, default: 'Person' },

    givenName: String,
    familyName: String,
    image: { type: String, default: null },
    password: String,
    role: {type: String, default: 'basic', enum:["basic", "admin"]},
    email: { unique: true, type: String },
    creationDate: { type: Date, default: new Date() },
    banished: { type: Boolean, default: false }
})

MySchema.methods.generateJwt = user => {
    // Setting available duration
    const expiryToken = new Date();
    expiryToken.setDate( expiryToken.getDate() + 59 );

    // Setting Token
    const jwtObject = {
        _id: user._id,
        email: user.email,
        password: user.password,
        banished: user.banished,        
        expireIn: '10s',
        exp: parseInt( expiryToken.getTime() / 100, 10 )
    }

    // Return JsonWebToken
    return jwt.sign( jwtObject, process.env.JWT_SECRET );
}

module.exports = mongoose.model('user', MySchema)