/*
Import
*/
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Definition
*/
const MySchema = new Schema({
    '@context': { type: String, default: 'http://schema.org' },
    '@type': { type: String, default: 'Film' },

    comments: [{ type: Schema.Types.ObjectId, ref: 'comment'}],
})
//

/* 
Export
*/
module.exports = mongoose.model('movie', MySchema)
//
