/*
Import
*/
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Definition
*/
const MySchema = new Schema({
    '@context': { type: String, default: 'http://schema.org' },
    '@type': { type: String, default: 'Commentaire' },

    title: String,
    content: String,
    movieId: Number,

    // Binding to a User id
    author: { type: Schema.Types.ObjectId, ref: 'user'},
    // Binding to a Movie id
    movie: { type: Schema.Types.ObjectId, ref: 'movie'},

    // Default value
    creationDate: { type: Date, default: new Date() },
    dateModified: { type: Date, default: new Date() },
    isPublished: { type: Boolean, default: false }
})
//

/* 
Export
*/
module.exports = mongoose.model('comment', MySchema)
//
