const express = require('express');
const { checkFields } = require('../services/request.service');
const Mandatory = require('../services/mandatory.service');
const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service')
const fetch = require('node-fetch');

// Import controller(s)
const Controllers = require('../controllers/index')

class ApiRouter {
    constructor( { passport } ){
        this.router = express.Router(); 
        this.passport = passport
    }

    routes(){
        // Defining index route
        this.router.get('/', (req, res) => {
            return res.json({ definition: 'TODO: add API definition' })
        })

        // Route to create object
        this.router.post('/:endpoint', this.passport.authenticate('jwt', { session: false }), (req, res) => {
            // Check body data
            if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
                return sendBodyError(`/api/${req.params.endpoint}`, 'POST', res, 'No data provided in the reqest body')
            }
            else{
                // Check body data
                const { ok, extra, miss } = checkFields( Mandatory[req.params.endpoint], req.body );
                // Error 
                if( !ok ){ return sendFieldsError(`/api/${req.params.endpoint}`, 'POST', res, 'Bad fields provided', miss, extra) }
                else{
                    // Add author _id
                    req.body.author = req.user._id;

                    // Using the controller to create a new object
                    Controllers[req.params.endpoint].createOne(req)
                    .then( apiResponse => sendApiSuccessResponse(`/api/${req.params.endpoint}`, 'POST', res, 'Request succeed', apiResponse) )
                    .catch( apiError => sendApiErrorResponse(`/api/${req.params.endpoint}`, 'POST', res, 'Request failed', apiError) );
                }
            }
        })

        //Router to retrieve movies from TMDB
        this.router.get('/movie', (req, res) => {
            let url = `http://api.themoviedb.org/3/discover/movie?api_key=${process.env.MOVIE_API_KEY}&with_genres=27`;
            let settings = { method: "GET" };
            
            fetch(url, settings)
                .then(res => res.json())
                .then(data => {
                    let movies = data.results;
                    res.send({movies})
            })
        })

        // Route to real all objects
        this.router.get('/:endpoint', (req, res) => {
            Controllers[req.params.endpoint].readAll()
            .then( apiResponse => sendApiSuccessResponse(`/api/${req.params.endpoint}`, 'GET', res, 'Request succeed', apiResponse) )
            .catch( apiError => sendApiErrorResponse(`/api/${req.params.endpoint}`, 'GET', res, 'Request failed', apiError) );
        })

        //Route to retrieve movie details from TMDB
        this.router.get('/movie/:id', (req, res) => {
            let url = `http://api.themoviedb.org/3/movie/${req.params.id}?api_key=${process.env.MOVIE_API_KEY}&with_genres=27`;
            let settings = { method: "GET" };
            
            fetch(url, settings)
                .then(res => res.json())
                .then(data => res.send({data})   
            )  
        })

        // Route to read one object
        this.router.get('/:endpoint/:id', (req, res) => {
            // Using the controller to get data
            Controllers[req.params.endpoint].readOne(req.params.id)
            .then( apiResponse => sendApiSuccessResponse(`/api/${req.params.endpoint}/${req.params.id}`, 'GET', res, 'Request succeed', apiResponse) )
            .catch( apiError => sendApiErrorResponse(`/api/${req.params.endpoint}/${req.params.id}`, 'GET', res, 'Request failed', apiError) );
        })

        // Route to update one object
        this.router.put('/:endpoint/:id', this.passport.authenticate('jwt', { session: false }), (req, res) => {
            // Check body data
            if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
                return sendBodyError(`/api/${req.params.endpoint}/${req.params.id}`, 'PUT', res, 'No data provided in the request body')
            }
            else{
                // Check body data
                const { ok, extra, miss } = checkFields( Mandatory[req.params.endpoint], req.body );

                // Error: bad fields provided
                if( !ok ){  
                    return sendFieldsError(`/api/${req.params.endpoint}/${req.params.id}`, 'PUT', res, 'Bad fields provided', miss, extra) }
                
                else {
                    // Using the controller to update data
                    Controllers[req.params.endpoint].updateOne(req)
                    .then( apiResponse => sendApiSuccessResponse(`/api/${req.params.endpoint}/${req.params.id}`, 'PUT', res, 'Request succeed', apiResponse) )
                    .catch( apiError => sendApiErrorResponse(`/api/${req.params.endpoint}/${req.params.id}`, 'PUT', res, 'Request failed', apiError) );
                }
            }
        })

        // Route to delete one object
        this.router.delete('/:endpoint/:id', this.passport.authenticate('jwt', { session: false }), (req, res) => {
            // Using the controller to delete data
            Controllers[req.params.endpoint].deleteOne(req)
            .then( apiResponse => sendApiSuccessResponse(`/api/${req.params.endpoint}/${req.params.id}`, 'DELETE', res, 'Request succeed', apiResponse) )
            .catch( apiError => sendApiErrorResponse(`/api/${req.params.endpoint}/${req.params.id}`, 'DELETE', res, 'Request failed', apiError) );
        })
    }; 


    init(){
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

module.exports = ApiRouter;