const express = require('express');
const bcrypt = require('bcryptjs');
const Controllers = require('../controllers/index')
const { checkFields } = require('../services/request.service');
const Mandatory = require('../services/mandatory.service');
const { sendBodyError,sendFieldsError,sendApiSuccessResponse,sendApiErrorResponse } = require('../services/response.service');


class RouterClass {
    constructor( { passport } ){
        this.passport = passport
        this.router = express.Router(); 
    }

    routes(){
        this.router.post('/register', async (req, res) => {
            // Check body data
            if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
                return sendBodyError('/auth/register', 'POST', res, 'No data provided in the reqest body')
            }
            else{
                // Check body data
                const { ok, extra, miss } = checkFields( Mandatory.register, req.body );

                // Error
                if( !ok ){ return sendFieldsError('/auth/register', 'POST', res, 'Bad fields provided', miss, extra) }
                else{
                    Controllers.auth.register(req)
                    .then( data => {
                        return sendApiSuccessResponse('/auth/register', 'POST', res, 'Request succeed', data)
                    } )
                    .catch( err => sendApiErrorResponse('/auth/register', 'POST', res, 'Request failed', err) );
                }
            }
        })

        // AUTH: Login
        this.router.post('/login', (req, res) => {
            // Check body data
            if( typeof req.body === 'undefined' || req.body === null || Object.keys(req.body).length === 0 ){ 
                return sendBodyError('/auth/login', 'POST', res, 'No data provided in the reqest body')
            }
            else{
                // Check body data
                const { ok, extra, miss } = checkFields( Mandatory.login, req.body );

                // Error
                if( !ok ){ return sendFieldsError('/auth/login', 'POST', res, 'Bad fields provided', miss, extra) }
                else{
                    Controllers.auth.login(req, res)
                    .then( apiResponse => sendApiSuccessResponse('/auth/login', 'POST', res, 'User logged', apiResponse) )
                    .catch( apiError => sendApiErrorResponse('/auth/login', 'POST', res, 'Request error', apiError) );
                }
            }
        })

        this.router.get('/me', this.passport.authenticate('jwt', { session: false }), (req, res) => {
            sendApiSuccessResponse('/auth/login', 'POST', res, 'User logged', req.user)
        })

        this.router.get('/logout', (req, res) => {
            Controllers.auth.logout(req, res)
            .then( apiResponse => sendApiSuccessResponse('/auth/logout', 'GET', res, 'Request succeed', apiResponse) )
            .catch( apiError => sendApiErrorResponse('/auth/logout', 'GET', res, 'Request failed', apiError) );
        })

    }

    init(){
        // Get route fonctions
        this.routes();

        // Sendback router
        return this.router;
    };
}

module.exports = RouterClass;
